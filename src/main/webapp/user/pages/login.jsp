<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Struts2 Example</title>
</head>
<body>
	
	<div style="margin-bottom: 10px;">
		<h3>Info:</h3>
		<label>Username: xpto</label>
		<br/>
		<label>Password: xpto</label>
		<br/>
	</div>
	
	<s:form action="welcome">
		<s:textfield name="username" label="Username"></s:textfield>
		<s:password name="password" label="Password" type="password"></s:password>
		<s:submit value="Enviar"></s:submit>
	</s:form>

</body>
</html>