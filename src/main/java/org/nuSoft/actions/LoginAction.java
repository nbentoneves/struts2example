package org.nuSoft.actions;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.ResultPath;

import com.opensymphony.xwork2.ActionSupport;

@Namespace("/user")
@ResultPath(value="/")
@Result(name="success", location="pages/login.jsp")
public class LoginAction extends ActionSupport {

	/**
	 * Serial Version ID 
	 */
	private static final long serialVersionUID = -73751119323951948L;

	
}
