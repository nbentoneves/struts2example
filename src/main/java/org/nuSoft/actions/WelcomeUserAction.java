package org.nuSoft.actions;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.ResultPath;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @ResultPath permit application find the pages out of default folder:
 *             /Struts2Example/WEB-INF/content/User/login.jsp
 * 
 *             If define the ResultPath it will go first @Namespace and next it
 *             will takes the location on @Result tag:
 *             /Struts2Example/User/pages/...
 * 
 * @author nuno.castro
 * 
 */
@Namespace("/user")
@ResultPath(value = "/")
@Results(value = {
		@Result(name = "success", location = "pages/welcome_user.jsp"),
		@Result(name = "error", location = "pages/error_user.jsp") })
@ExceptionMappings(value = { @ExceptionMapping(exception = "java.lang.NullPointerException", result = "error") })
public class WelcomeUserAction extends ActionSupport {

	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = 1047500016785957621L;

	private String username;

	private String password;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Action(value = "welcome")
	public String executeWelcome() {

		if (username.equalsIgnoreCase("xpto")
				&& password.equalsIgnoreCase("xpto")) {
			return SUCCESS;
		}

		return ERROR;
	}

}
